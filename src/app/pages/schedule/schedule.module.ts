import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SchedulePage } from './schedule';
import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { ScheduleFilterPagelist } from '../schedule-filterlist/schedule-filterlist';
import { SchedulePageRoutingModule } from './schedule-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SchedulePageRoutingModule
  ],
  declarations: [
    SchedulePage,
    ScheduleFilterPage,
    ScheduleFilterPagelist
  ],
  entryComponents: [
    ScheduleFilterPage,
    ScheduleFilterPagelist
  ]
})
export class ScheduleModule { }
