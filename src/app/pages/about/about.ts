import { Component, ViewEncapsulation,ViewChild, ElementRef } from '@angular/core';

import { PopoverController,ModalController } from '@ionic/angular';
import {ScheduleFilterPagelist} from '../schedule-filterlist/schedule-filterlist'

import { PopoverPage } from '../about-popover/about-popover';
import { Chart } from 'chart.js';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  styleUrls: ['./about.scss'],
})
export class AboutPage {
  myDate = '2047-05-17';
  public modalCtrl: ModalController;
  excludeTracks: any = [];

  private barChart: Chart;
  private doughnutChart: Chart;
  private lineChart: Chart;

  @ViewChild("barCanvas", {static: false}) barCanvas: ElementRef;
  // @ViewChild("doughnutCanvas", {static: false}) doughnutCanvas: ElementRef;
  @ViewChild("lineCanvas", {static: false}) lineCanvas: ElementRef;

  constructor(public popoverCtrl: PopoverController) { }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }

  async presentFilterlist() {
    const modal = await this.modalCtrl.create({
      component: ScheduleFilterPagelist,
      componentProps: { excludedTracks: this.excludeTracks }
    });
    await modal.present();

    // const { data } = await modal.onWillDismiss();
    // if (data) {
    //   this.excludeTracks = data;
    //   this.updateSchedule();
    // }
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
          {
            label: "My Deals",
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            borderColor: [
              "rgba(255,99,132,1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)"
            ],
            borderWidth: 1
          }
        ]
      },
      options: {
        legend: {
          labels: {
            fontColor: "#f9f7f7"
          },
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                fontColor: "#f9f7f7",
              }
            }
          ], xAxes: [
            {
              ticks: {
                fontColor: "#f9f7f7",
              }
            }
          ]
        }
      }
    });

  
  this.lineChart = new Chart(this.lineCanvas.nativeElement, {
    type: "line",
    data: {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          fontColor:   "rgba(249, 247, 247, 0.8)",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(250,250,250,0.5)",//"rgba(75,192,192,1)"
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgba(75,192,192,1)",
          pointBackgroundColor: "rgba(255,250,250,0.9)",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [65, 59, 80, 81, 56, 55, 40],
          spanGaps: false
        }
      ],
    },
      options: {
        legend: {
          labels: {
            fontColor: "#f9f7f7"
          },
        },
        scales: {
          yAxes: [
            {
              ticks: {
                fontColor: "#f9f7f7",
              }
            }
          ], xAxes: [
            {
              ticks: {
                fontColor: "#f9f7f7",
              }
            }
          ]
        }
      }
  });
}
}
