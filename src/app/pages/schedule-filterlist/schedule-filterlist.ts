import { AfterViewInit, Component } from '@angular/core';
import { Config, ModalController, NavParams } from '@ionic/angular';

import { ConferenceData } from '../../providers/conference-data';


@Component({
  selector: 'page-schedule-filterlist',
  templateUrl: 'schedule-filterlist.html',
  styleUrls: ['./schedule-filterlist.scss'],
})
export class ScheduleFilterPagelist implements AfterViewInit {
  ios: boolean;

  tracks: {name: string, icon: string, isChecked: boolean}[] = [];

  dealsprofile:{name:string,mob:string,amount:string,vehicletype:string}[]=[];

  constructor(
    public confData: ConferenceData,
    private config: Config,
    public modalCtrl: ModalController,
    public navParams: NavParams
  ) { }

  ionViewWillEnter() {
    this.ios = this.config.get('mode') === `ios`;
  }

  // TODO use the ionViewDidEnter event
  ngAfterViewInit() {
    // passed in array of track names that should be excluded (unchecked)
    const excludedTrackNames = this.navParams.get('excludedTracks');

    // this.confData.getTracks().subscribe((tracks: any[]) => {
    //   tracks.forEach(track => {
    //     this.tracks.push({
    //       name: track.name,
    //       icon: track.icon,
    //       isChecked: (excludedTrackNames.indexOf(track.name) === -1)
    //     });
    //   });
    // });

    this.dealsprofile.push({
      name:'Logan Morrow',
      mob:'767-898-0110',
      amount:'1200',
      vehicletype:'toyota camry'
    },
    {
      name:'Ari smalls',
      mob:'832-799-0110',
      amount:'1400',
      vehicletype:'honda civic'
    },
    {
      name:'Rubix cube',
      mob:'767-898-8765',
      amount:'900',
      vehicletype:'benz'
    },
    {
      name:'Nicki petiz',
      mob:'646-909-8110',
      amount:'1800',
      vehicletype:'Audi'
    },
    {
      name:'Dwayne robinson',
      mob:'347-898-0890',
      amount:'1140',
      vehicletype:'bmw x10'
    },
    {
      name:'ryan kyre',
      mob:'437-678-0220',
      amount:'2500',
      vehicletype:'Mustang'
    })
  }

  selectAll(check: boolean) {
    // set all to checked or unchecked
    this.tracks.forEach(track => {
      track.isChecked = check;
    });
  }

  applyFilters() {
    // Pass back a new array of track names to exclude
    const excludedTrackNames = this.tracks.filter(c => !c.isChecked).map(c => c.name);
    this.dismiss(excludedTrackNames);
  }

  dismiss(data?: any) {
    // using the injected ModalController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(data);
  }
}
